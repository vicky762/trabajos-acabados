#ifndef Dakeypad_h
#define Dakeypad_h

#include "Arduino.h"

class keypd//keypad class
{
  public:
  
    keypd(int r0,int r1,int r2,int r3,int c0,int c1,int c2,int c3);//defines rows and cols pins
    char getch();//gets a single char pressed from de keypad. If none, returns 'n'
    int confirmkey(int n);//Just a test function
    int chrtoint(char input);//converts a single char into an int. If char is not a number, returns -1
    long getnum();//gets a whole number introducing the digits one by one. Once '#' is pressed, returns the final number
    void getnum2(long *current, byte _reboot);
  private:
  
  int _r[4]={0,0,0,0};//This array stores the pins connected to rows (in order). Requires to use keypd first
  int _c[4]={0,0,0,0};//Same as before but with the cols
  char keys[4][4]={//chars associated to each button
    {'1','2','3','A'},
    {'4','5','6','B'},
    {'7','8','9','C'},
    {'*','0','#','D'},  
  };
};

#endif
