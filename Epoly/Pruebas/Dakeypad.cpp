#include "Arduino.h"
#include "Dakeypad.h"

keypd::keypd(int r0,int r1,int r2,int r3,int c0,int c1,int c2,int c3){
  int i=0;
  _r[0]=r0;
  _r[1]=r1;
  _r[2]=r2;
  _r[3]=r3;
  _c[0]=c0;
  _c[1]=c1;
  _c[2]=c2;
  _c[3]=c3;
  for(i=0;i<4;i++){
    pinMode(_r[i],OUTPUT);
    pinMode(_c[i],INPUT_PULLUP);
  }
}


char keypd::getch(){
  char ch='n';
  int r,c;
  digitalWrite(_r[0],HIGH);
  digitalWrite(_r[1],HIGH);
  digitalWrite(_r[2],HIGH);
  digitalWrite(_r[3],HIGH);
  for(r=0;r<4;r++){
    digitalWrite(_r[r],LOW);
    for(c=0;c<4;c++){
      if(digitalRead(_c[c])==LOW){
        ch=keys[r][c];
        while(digitalRead(_c[c])==LOW);
        break;
      }
    }
    digitalWrite(_r[r],HIGH);
  }
  return ch;
}
int keypd::confirmkey(int n){
  int nkey;
  if(n<4){
    nkey=_r[n];
  }
  else if (n>=4 && n<8){
    nkey=_c[n-4];
  }
  else{
    nkey=-1;
  }
  return nkey;
}


int keypd::chrtoint(char input){
  int num=-1;
  num=input-48;
  if(num<0 or num>9){
    num=-1;
  }
  return num;
}


long keypd::getnum(){
  int _on=1,i=0;
  long num=0;
  char ch='n';
  int n=0;
  while(_on==1){
    ch=getch();
    n=chrtoint(ch);
    if(ch!='n'){
      if(ch=='#'){
        _on=0;
      }
      else if(n>=0){
        if(i>0){
          num*=10;
        }
        num+=n;
        i++;
      }
    }
  }
  return num;
}
void keypd::getnum2(long *currentnum, byte _reboot){
  int i,_on=1,n=-1;
  char ch='n';
  if(!_reboot){
    currentnum=0;
    i=0;
  }
  else{
    ch=getch();
    n=chrtoint(ch);
    if(n>=0){
      if(i>0){
        *currentnum*=10;
      }
      *currentnum+=n;
      i++;
    }
  }
}
