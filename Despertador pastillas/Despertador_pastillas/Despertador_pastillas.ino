#include <NTPClient.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

const char *ssid     = "PATATA"; //SSID O NOMBRE DE VUESTRA RED
const char *password = "JOHNSON"; //CONTRASEÑA DE VUESTRA RED

const long utcOffsetInSeconds = 3600;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);

const int led=5; //D1
const int puls=13;//D7 en PULLDOWN
int _time[2]={0,0};

byte check[2]={0,0};

byte period=0;
//CONSUMO DE 81.5~82.0 mA a 5V 
void setup() {
  Serial.begin(115200);
  pinMode(led,OUTPUT);
  pinMode(puls,INPUT);
  WiFi.begin(ssid, password);
  //Mientras busca la red wifi, el led parpadea para informar al usuario
  while ( WiFi.status() != WL_CONNECTED ) {
    delay (250);
    Serial.print ( "." );
    digitalWrite(led,HIGH);
    delay(250);
    digitalWrite(led,LOW);
  }
  digitalWrite(led,HIGH);
  timeClient.begin();
}

void loop() {
  timeClient.update();
  _time[0]=timeClient.getHours();
  _time[1]=timeClient.getMinutes();
  //carga la hora y los minutos
  /* Eliminar los comentarios (L39 y L51) para activar los mensajes en puerto serie
  if(!millis()%1000){
    Serial.print(_time[0]);
    Serial.print(":");
    Serial.println(_time[1]);
    Serial.print("Periodo: ");
    Serial.println(period);
    Serial.print("AM: ");
    Serial.println(check[0]);
    Serial.print("PM: ");
    Serial.println(check[1]);
  }
  */
  if(_time[0]==6&&_time[1]==0){//Establece el periodo 1, y reinicia el estado del led
    check[0]=0;
    period=0;
  }
  if(_time[0]==21&&_time[1]==0){
    check[1]=0;
    period=1;
  }
  if(!check[period]){
    digitalWrite(led,HIGH);
    if(digitalRead(puls)==HIGH){
      check[period]=1;
    }
  }
  else{
    digitalWrite(led,LOW);
  }
}
